# Head First REST API

## Dev

- Host : http://headfirst-restapi.local/

### Install composer

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
cp composer.phar composer
```

```bash
echo @php "%~dp0composer" %*>composer.bat
```

Copy composer.bat & composer.phar && composer to your %PHP_HOME%

### Install REST API Package

- https://lumen.laravel.com/docs/5.7

Make sure your composer vendor bin added to your path

```bash
$ echo $COMPOSER_HOME
C:\Users\Administrator\AppData\Roaming\Composer
```

## Documents

- https://www.restapitutorial.com/
- https://www.youtube.com/watch?v=ZpqCN8iO9Y8

## Running

```
composer install
php -S localhost:8000 -t public
```

# Development

1.First step

- https://seegatesite.com/restful-api-tutorial-with-lumen-laravel-5-5-for-beginners/
- Docs : https://laravel.com/api/5.7/index.html

```bash
cp .env.example .env
php artisan migrate
```